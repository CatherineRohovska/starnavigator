//
//  ConstellationDescription.h
//  StarNavigator
//
//  Created by User on 10/7/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
typedef struct{
    GLKVector4 firstCoodinate;
    GLKVector4 secondCoordinate;
}pairPoints;
@interface ConstellationDescription : NSObject
{
      NSMutableDictionary* constellAbbreviationToFullNames;
}
@property (nonatomic, readonly) NSString* name;
@property (nonatomic, readonly) NSString* fullName;
@property (nonatomic, readonly) int* starsIndexes;
@property (nonatomic, readonly) GLKVector3 coordinatesCenter;
@property (nonatomic, readonly) int starsCount;
@property (nonatomic, readonly) NSMutableArray* starsOrder;
@property (nonatomic, readonly) pairPoints* linesCoord;
-(void) setConstellation: (NSMutableArray*) data;
@end
