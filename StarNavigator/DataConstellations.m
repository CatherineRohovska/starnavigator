//
//  DataConstellations.m
//  StarNavigator
//
//  Created by User on 10/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "DataConstellations.h"

@implementation DataConstellations
+(id) sharedStorage{
    static DataConstellations *sharedStorage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStorage = [[self alloc] init];
    });
    return sharedStorage;
}
- (id)init {
    if (self = [super init]) {
        NSError *error = nil;
        NSString* jsonData = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"constellationInfo" ofType:nil] encoding:NSUTF8StringEncoding error:&error];
        //creating data
        NSDictionary * allData = [NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
        //parsing data to array
        _constellationsCatalog = [allData objectForKey:@"Constellations"];
        [self createDictionaryOfConstellations];
    }
    return  self;
}

-(void) createDictionaryOfConstellations
{
    _constellAbbreviationToFullNames = [NSMutableDictionary dictionary];
    [_constellAbbreviationToFullNames setObject:@"Andromeda" forKey:@"And"];
    [_constellAbbreviationToFullNames setObject:@"Antlia" forKey:@"Ant"];
    [_constellAbbreviationToFullNames setObject:@"Apus" forKey:@"Aps"];
    [_constellAbbreviationToFullNames setObject:@"Aries" forKey:@"Ari"];
    [_constellAbbreviationToFullNames setObject:@"Aquarius" forKey:@"Aqr"];
    [_constellAbbreviationToFullNames setObject:@"Aquila" forKey:@"Aql"];
    [_constellAbbreviationToFullNames setObject:@"Ara" forKey:@"Ara"];
    [_constellAbbreviationToFullNames setObject:@"Auriga" forKey:@"Aur"];
    [_constellAbbreviationToFullNames setObject:@"Bootes" forKey:@"Boo"];
    [_constellAbbreviationToFullNames setObject:@"Caelum" forKey:@"Cae"];
    [_constellAbbreviationToFullNames setObject:@"Camelopardalis" forKey:@"Cam"];
    [_constellAbbreviationToFullNames setObject:@"Cancer" forKey:@"Cnc"];
    [_constellAbbreviationToFullNames setObject:@"Canes Venatici" forKey:@"CVn"];
    [_constellAbbreviationToFullNames setObject:@"Canis Major" forKey:@"CMa"];
    [_constellAbbreviationToFullNames setObject:@"Canis Minor" forKey:@"CMi"];
    [_constellAbbreviationToFullNames setObject:@"Capricornus" forKey:@"Cap"];
    [_constellAbbreviationToFullNames setObject:@"Carina" forKey:@"Car"];
    [_constellAbbreviationToFullNames setObject:@"Cassiopeia" forKey:@"Cas"];
    [_constellAbbreviationToFullNames setObject:@"Centaurus" forKey:@"Cen"];
    [_constellAbbreviationToFullNames setObject:@"Cepheus" forKey:@"Cep"];
    [_constellAbbreviationToFullNames setObject:@"Cetus" forKey:@"Cet"];
    [_constellAbbreviationToFullNames setObject:@"Circinus" forKey:@"Cir"];
    [_constellAbbreviationToFullNames setObject:@"Chamaeleon" forKey:@"Cha"];
    [_constellAbbreviationToFullNames setObject:@"Columba" forKey:@"Col"];
    [_constellAbbreviationToFullNames setObject:@"Coma Berenices" forKey:@"Com"];
    [_constellAbbreviationToFullNames setObject:@"Corona Australis" forKey:@"CrA"];
    [_constellAbbreviationToFullNames setObject:@"Corona Borealis" forKey:@"CrB"];
    [_constellAbbreviationToFullNames setObject:@"Corvus" forKey:@"Crv"];
    [_constellAbbreviationToFullNames setObject:@"Crater" forKey:@"Crt"];
    [_constellAbbreviationToFullNames setObject:@"Crux" forKey:@"Cru"];
    [_constellAbbreviationToFullNames setObject:@"Cygnus" forKey:@"Cyg"];
    [_constellAbbreviationToFullNames setObject:@"Delphinus" forKey:@"Del"];
    [_constellAbbreviationToFullNames setObject:@"Dorado" forKey:@"Dor"];
    [_constellAbbreviationToFullNames setObject:@"Draco" forKey:@"Dra"];
    [_constellAbbreviationToFullNames setObject:@"Equuleus" forKey:@"Equ"];
    [_constellAbbreviationToFullNames setObject:@"Eridanus" forKey:@"Eri"];
    [_constellAbbreviationToFullNames setObject:@"Fornax" forKey:@"For"];
    [_constellAbbreviationToFullNames setObject:@"Gemini" forKey:@"Gem"];
    [_constellAbbreviationToFullNames setObject:@"Grus" forKey:@"Gru"];
    [_constellAbbreviationToFullNames setObject:@"Hercules" forKey:@"Her"];
    [_constellAbbreviationToFullNames setObject:@"Horologium" forKey:@"Hor"];
    [_constellAbbreviationToFullNames setObject:@"Hydra" forKey:@"Hya"];
    [_constellAbbreviationToFullNames setObject:@"Hydrus" forKey:@"Hyi"];
    [_constellAbbreviationToFullNames setObject:@"Indus" forKey:@"Ind"];
    [_constellAbbreviationToFullNames setObject:@"Lacerta" forKey:@"Lac"];
    [_constellAbbreviationToFullNames setObject:@"Leo" forKey:@"Leo"];
    [_constellAbbreviationToFullNames setObject:@"Leo Minor" forKey:@"LMi"];
    [_constellAbbreviationToFullNames setObject:@"Lepus" forKey:@"Lep"];
    [_constellAbbreviationToFullNames setObject:@"Libra" forKey:@"Lib"];
    [_constellAbbreviationToFullNames setObject:@"Lupus" forKey:@"Lup"];
    [_constellAbbreviationToFullNames setObject:@"Lynx" forKey:@"Lyn"];
    [_constellAbbreviationToFullNames setObject:@"Lyra" forKey:@"Lyr"];
    [_constellAbbreviationToFullNames setObject:@"Mensa" forKey:@"Men"];
    [_constellAbbreviationToFullNames setObject:@"Microscopium" forKey:@"Mic"];
    [_constellAbbreviationToFullNames setObject:@"Monoceros" forKey:@"Mon"];
    [_constellAbbreviationToFullNames setObject:@"Musca" forKey:@"Mus"];
    [_constellAbbreviationToFullNames setObject:@"Norma" forKey:@"Nor"];
    [_constellAbbreviationToFullNames setObject:@"Octans" forKey:@"Oct"];
    [_constellAbbreviationToFullNames setObject:@"Ophiuchus" forKey:@"Oph"];
    [_constellAbbreviationToFullNames setObject:@"Orion" forKey:@"Ori"];
    [_constellAbbreviationToFullNames setObject:@"Pavo" forKey:@"Pav"];
    [_constellAbbreviationToFullNames setObject:@"Pegasus" forKey:@"Peg"];
    [_constellAbbreviationToFullNames setObject:@"Perseus" forKey:@"Per"];
    [_constellAbbreviationToFullNames setObject:@"Phoenix" forKey:@"Phe"];
    [_constellAbbreviationToFullNames setObject:@"Pictor" forKey:@"Pic"];
    [_constellAbbreviationToFullNames setObject:@"Pisces" forKey:@"Psc"];
    [_constellAbbreviationToFullNames setObject:@"Piscis Austrinus" forKey:@"PsA"];
    [_constellAbbreviationToFullNames setObject:@"Puppis" forKey:@"Pup"];
    [_constellAbbreviationToFullNames setObject:@"Pyxis" forKey:@"Pyx"];
    [_constellAbbreviationToFullNames setObject:@"Reticulum" forKey:@"Ret"];
    [_constellAbbreviationToFullNames setObject:@"Sagitta" forKey:@"Sge"];
    [_constellAbbreviationToFullNames setObject:@"Sagittarius" forKey:@"Sgr"];
    [_constellAbbreviationToFullNames setObject:@"Scorpio" forKey:@"Sco"];
    [_constellAbbreviationToFullNames setObject:@"Sculptor" forKey:@"Scl"];
    [_constellAbbreviationToFullNames setObject:@"Scutum" forKey:@"Sct"];
    [_constellAbbreviationToFullNames setObject:@"Serpens" forKey:@"Ser"];
    [_constellAbbreviationToFullNames setObject:@"Sextans" forKey:@"Sex"];
    [_constellAbbreviationToFullNames setObject:@"Taurus" forKey:@"Tau"];
    [_constellAbbreviationToFullNames setObject:@"Telescopium" forKey:@"Tel"];
    [_constellAbbreviationToFullNames setObject:@"Triangulum" forKey:@"Tri"];
    [_constellAbbreviationToFullNames setObject:@"Triangulum Australe" forKey:@"TrA"];
    [_constellAbbreviationToFullNames setObject:@"Tucana" forKey:@"Tuc"];
    [_constellAbbreviationToFullNames setObject:@"Ursa Major" forKey:@"UMa"];
    [_constellAbbreviationToFullNames setObject:@"Ursa Minor" forKey:@"UMi"];
    [_constellAbbreviationToFullNames setObject:@"Vela" forKey:@"Vel"];
    [_constellAbbreviationToFullNames setObject:@"Virgo" forKey:@"Vir"];
    [_constellAbbreviationToFullNames setObject:@"Volans" forKey:@"Vol"];
    [_constellAbbreviationToFullNames setObject:@"Vulpecula" forKey:@"Vul"];
    
}
@end
