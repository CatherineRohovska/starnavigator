//
//  DataStorage.m
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "DataStorage.h"

const float minAlpha = 0.3;
const float maxAlpha = 1;
const float minSize = 2;
const float maxSize = 5;
@implementation DataStorage
+(id) sharedStorage{
    static DataStorage *sharedStorage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStorage = [[self alloc] init];
    });
    return sharedStorage;
}
-(void) getUrlData: (NSData*) dataSample
{
    NSData *receivedData;
    receivedData= dataSample;
    //creating data
    NSDictionary * allData = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:nil];
    //parsing data to array
    NSArray* tmpData = [allData objectForKey:@"object"];
    //        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"Name" ascending:NO];
    //        tmpData=[tmpData sortedArrayUsingDescriptors:@[sort]];
    _data = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<tmpData.count; i++)
    {
        StarDescription* tmpStar = [[StarDescription alloc] init];
        [tmpStar setStar:tmpData[i] index:i];
        [self.data addObject:tmpStar];
    }
    //stars data to render
    
    _graphicData = malloc(self.data.count*sizeof(graphicStarData));
    
    
    for (int i = 0; i<self.data.count; i++)
    {
        StarDescription* star = [self.data objectAtIndex:i];
        // float tmp = fabs(Rsphere*cos((star.DEd)*M_PI/180));
        _graphicData[i].position.x = star.celestialCoordinates.x;//sin(M_PI - M_PI/180*(star.RAh*15-23))*tmp;
        _graphicData[i].position.y = star.celestialCoordinates.y;//Rsphere*sin((star.DEd)*M_PI/180);
        _graphicData[i].position.z = star.celestialCoordinates.z;//cos(M_PI/180*(star.RAh*15-23))*tmp;
        _graphicData[i].position.w = 1.0;
        
        _graphicData[i].pointSize = fabs(fabs(star.vmagnitude-5.0)-(6.5-maxSize))+minSize; //scales the min size too
        
        StarDescription* item = [self.data objectAtIndex:i];
        NSString *firstLetter = [item.spectralClass substringToIndex:1 /*substringFromIndex:0*/];
        //  NSLog(@"%@", firstLetter);
        GLKVector4 color;
        float alpha =fabs(fabs(star.vmagnitude-5.0)/5 - 0.3)+0.1;
        if ([firstLetter isEqualToString:@"W"]) {color.r = 10/255.0; color.g = 234/255.0; color.b = 250/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"O"]) {color.r = 82/255.0; color.g = 243/255.0; color.b = 255/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"B"]) {color.r = 163/255.0; color.g = 249/255.0; color.b = 255/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"A"]) {color.r = 255/255.0; color.g = 255/255.0; color.b = 255.0/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"F"]) {color.r = 240/255.0; color.g = 234/255.0; color.b = 134/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"G"]) {color.r = 255/255.0; color.g = 244/255.0; color.b = 28/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"K"]) {color.r = 255/255.0; color.g = 142/255.0; color.b = 28/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"M"]) {color.r = 255/255.0; color.g = 36/255.0; color.b = 28/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"L"]) {color.r = 163/255.0; color.g = 16/255.0; color.b = 11/255.0; color.a = alpha; }
        if ([firstLetter isEqualToString:@"T"]) {color.r = 130/255.0; color.g = 63/255.0; color.b = 51/255.0; color.a = alpha; }
        //
        //             if ([item.constellation isEqualToString:@"UMa"] & (item.vmagnitude<=3.4)) {color.r = 0.47; color.g = 0.03; color.b = 0.67; color.a = 1;}
        _graphicData[i].color = color;
        
    }
    
    self.constellations = [NSMutableArray new];
    NSMutableSet* tmpConstellNames = [self getConstellationNames];
    //
    //
    for (int i=0; i<tmpConstellNames.count; i++) {
        ConstellationDescription* constell = [ConstellationDescription new];
        [constell setConstellation:[self getConstellation:[tmpConstellNames allObjects][i] ]];
        [self.constellations addObject:constell];
        //
    }
    //sort constellations in array by object field "Name"
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _constellations = (NSMutableArray*)[_constellations sortedArrayUsingDescriptors:sortDescriptors];
    
}
- (id)init {
    if (self = [super init]) {
        //creating translation from abreviations;
        //[self createDictionaryOfConstellations];
        //creating url
        
        //
        NSURL *sourceURL = [NSURL URLWithString:@"http://www.lizard-tail.com/isana/lab/astro_calc/js/bsc_json_short.txt"];
        //loading data
//        NSData* sourceData = [NSData dataWithContentsOfURL:sourceURL];
        NSURLRequest* urlRequest = [[NSURLRequest alloc] initWithURL:sourceURL];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSURLSession* currentSession = [NSURLSession sharedSession];
            NSURLSessionTask* task = [currentSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                [self getUrlData:data];
            }];
             [task resume];
        });
      
//        NSURLSessionTask* task = [currentSession  downloadTaskWithRequest:urlRequest completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        
//            [self getUrlData:location];
//            
//        }];
       
//     receivedData = [NSURLConnection sendSynchronousRequest:urlRequest
//                                                     returningResponse:&response
//                                                                 error:&error];
        
      
    }
    return self;
}
-(NSMutableArray*) getConstellation: (NSString*) name
{
    NSMutableArray* starsArray = [NSMutableArray new];
    for (int i = 0; i<self.data.count; i++)
    {
        StarDescription* star = [self.data objectAtIndex:i];
        if ([star.constellation isEqualToString:name])
        {
            [starsArray addObject:star];
        }
    }
    return starsArray;
}
-(NSMutableArray*) getData{
    return self.data;
}
- (NSMutableSet*) getConstellationNames
{
    NSMutableSet* constellSet = [NSMutableSet new];
    for (int i = 0; i<self.data.count; i++)
    {
        StarDescription* star = [self.data objectAtIndex:i];
      if(![star.constellation isEqualToString:@""])  [constellSet addObject:star.constellation];
    }
  
    return constellSet;
}

@end
