//
//  ConstellationDescription.m
//  StarNavigator
//
//  Created by User on 10/7/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ConstellationDescription.h"
#import "StarDescription.h"
#import "DataStorage.h"
#import "DataConstellations.h"
@implementation ConstellationDescription
-(void) setConstellation: (NSMutableArray*) data 
{
   // DataStorage* dataStorage = [DataStorage sharedStorage];
    float x, y, z;
    [self createDictionaryOfConstellations];
    x=y=z=0;
    _starsCount = (int)data.count;
    _starsIndexes = (malloc(sizeof(int)*data.count));
    for (int i=0; i<data.count; i++)
    {
     StarDescription* object = [data objectAtIndex:i];
        x = x+object.celestialCoordinates.x;
        y= y+ object.celestialCoordinates.y;
        z = z+ object.celestialCoordinates.z;
        _name = object.constellation;
        _starsIndexes[i] = object.starIndex;
        DataConstellations* dataConstellations = [DataConstellations sharedStorage];
        _fullName = [dataConstellations.constellAbbreviationToFullNames objectForKey:_name];
    }
    x= x/data.count*1.0;
    y= y/data.count*1.0;
    z= z/data.count*1.0;
    _coordinatesCenter.x =x;
    _coordinatesCenter.y = y;
    _coordinatesCenter.z = z;
    [self createStarsOrder: _fullName];
  
}

-(void) createStarsOrder: (NSString*) nameOfConstellation
{
    //get parsing data from array
    DataConstellations* dataConstellation = [DataConstellations sharedStorage];
    //create data from all dictionary
    for (int i=0; i<dataConstellation.constellationsCatalog.count; i++)
    {
        NSDictionary* currentConstell = dataConstellation.constellationsCatalog[i]; //tmpData[i];
        if ([[currentConstell objectForKey:@"Name"] isEqualToString:nameOfConstellation])
        {
            _starsOrder = [currentConstell objectForKey:@"lines"];
            NSArray* stars =  [currentConstell objectForKey:@"stars"];
            _linesCoord = malloc(sizeof(pairPoints)*_starsOrder.count);
            for (int j=0; j<_starsOrder.count; j++) {
                int firstIndex = (int)[[[_starsOrder objectAtIndex:j] objectAtIndex:0] integerValue]; //index first star
                int secondIndex = (int)[[[_starsOrder objectAtIndex:j] objectAtIndex:1] integerValue]; //index second star
                NSDictionary* firstStar = [stars objectAtIndex:firstIndex];
                NSDictionary* secondStar = [stars objectAtIndex:secondIndex];
                GLKVector4 firsrtCoord = [StarDescription calculateCoords:[[firstStar objectForKey:@"DEd"] floatValue]  Acs:[[firstStar objectForKey:@"RAh"] floatValue]];
                GLKVector4 secondCoord = [StarDescription calculateCoords:[[secondStar objectForKey:@"DEd"] floatValue]  Acs:[[secondStar objectForKey:@"RAh"] floatValue]];

          // create single line from pair of points and adding it to array
                pairPoints line;
                line.firstCoodinate = firsrtCoord;
                line.secondCoordinate = secondCoord;
                _linesCoord[j]=line;
                
            }
        }
        //
        
    }
   

}
-(void) createDictionaryOfConstellations
{
    constellAbbreviationToFullNames = [NSMutableDictionary dictionary];
    [constellAbbreviationToFullNames setObject:@"Andromeda" forKey:@"And"];
    [constellAbbreviationToFullNames setObject:@"Antlia" forKey:@"Ant"];
    [constellAbbreviationToFullNames setObject:@"Apus" forKey:@"Aps"];
    [constellAbbreviationToFullNames setObject:@"Aries" forKey:@"Ari"];
    [constellAbbreviationToFullNames setObject:@"Aquarius" forKey:@"Aqr"];
    [constellAbbreviationToFullNames setObject:@"Aquila" forKey:@"Aql"];
    [constellAbbreviationToFullNames setObject:@"Ara" forKey:@"Ara"];
    [constellAbbreviationToFullNames setObject:@"Auriga" forKey:@"Aur"];
    [constellAbbreviationToFullNames setObject:@"Bootes" forKey:@"Boo"];
    [constellAbbreviationToFullNames setObject:@"Caelum" forKey:@"Cae"];
    [constellAbbreviationToFullNames setObject:@"Camelopardalis" forKey:@"Cam"];
    [constellAbbreviationToFullNames setObject:@"Cancer" forKey:@"Cnc"];
    [constellAbbreviationToFullNames setObject:@"Canes Venatici" forKey:@"CVn"];
    [constellAbbreviationToFullNames setObject:@"Canis Major" forKey:@"CMa"];
    [constellAbbreviationToFullNames setObject:@"Canis Minor" forKey:@"CMi"];
    [constellAbbreviationToFullNames setObject:@"Capricornus" forKey:@"Cap"];
    [constellAbbreviationToFullNames setObject:@"Carina" forKey:@"Car"];
    [constellAbbreviationToFullNames setObject:@"Cassiopeia" forKey:@"Cas"];
    [constellAbbreviationToFullNames setObject:@"Centaurus" forKey:@"Cen"];
    [constellAbbreviationToFullNames setObject:@"Cepheus" forKey:@"Cep"];
    [constellAbbreviationToFullNames setObject:@"Cetus" forKey:@"Cet"];
    [constellAbbreviationToFullNames setObject:@"Circinus" forKey:@"Cir"];
    [constellAbbreviationToFullNames setObject:@"Chamaeleon" forKey:@"Cha"];
    [constellAbbreviationToFullNames setObject:@"Columba" forKey:@"Col"];
    [constellAbbreviationToFullNames setObject:@"Coma Berenices" forKey:@"Com"];
    [constellAbbreviationToFullNames setObject:@"Corona Australis" forKey:@"CrA"];
    [constellAbbreviationToFullNames setObject:@"Corona Borealis" forKey:@"CrB"];
    [constellAbbreviationToFullNames setObject:@"Corvus" forKey:@"Crv"];
    [constellAbbreviationToFullNames setObject:@"Crater" forKey:@"Crt"];
    [constellAbbreviationToFullNames setObject:@"Crux" forKey:@"Cru"];
    [constellAbbreviationToFullNames setObject:@"Cygnus" forKey:@"Cyg"];
    [constellAbbreviationToFullNames setObject:@"Delphinus" forKey:@"Del"];
    [constellAbbreviationToFullNames setObject:@"Dorado" forKey:@"Dor"];
    [constellAbbreviationToFullNames setObject:@"Draco" forKey:@"Dra"];
    [constellAbbreviationToFullNames setObject:@"Equuleus" forKey:@"Equ"];
    [constellAbbreviationToFullNames setObject:@"Eridanus" forKey:@"Eri"];
    [constellAbbreviationToFullNames setObject:@"Fornax" forKey:@"For"];
    [constellAbbreviationToFullNames setObject:@"Gemini" forKey:@"Gem"];
    [constellAbbreviationToFullNames setObject:@"Grus" forKey:@"Gru"];
    [constellAbbreviationToFullNames setObject:@"Hercules" forKey:@"Her"];
    [constellAbbreviationToFullNames setObject:@"Horologium" forKey:@"Hor"];
    [constellAbbreviationToFullNames setObject:@"Hydra" forKey:@"Hya"];
    [constellAbbreviationToFullNames setObject:@"Hydrus" forKey:@"Hyi"];
    [constellAbbreviationToFullNames setObject:@"Indus" forKey:@"Ind"];
    [constellAbbreviationToFullNames setObject:@"Lacerta" forKey:@"Lac"];
    [constellAbbreviationToFullNames setObject:@"Leo" forKey:@"Leo"];
    [constellAbbreviationToFullNames setObject:@"Leo Minor" forKey:@"LMi"];
    [constellAbbreviationToFullNames setObject:@"Lepus" forKey:@"Lep"];
    [constellAbbreviationToFullNames setObject:@"Libra" forKey:@"Lib"];
    [constellAbbreviationToFullNames setObject:@"Lupus" forKey:@"Lup"];
    [constellAbbreviationToFullNames setObject:@"Lynx" forKey:@"Lyn"];
    [constellAbbreviationToFullNames setObject:@"Lyra" forKey:@"Lyr"];
    [constellAbbreviationToFullNames setObject:@"Mensa" forKey:@"Men"];
    [constellAbbreviationToFullNames setObject:@"Microscopium" forKey:@"Mic"];
    [constellAbbreviationToFullNames setObject:@"Monoceros" forKey:@"Mon"];
    [constellAbbreviationToFullNames setObject:@"Musca" forKey:@"Mus"];
    [constellAbbreviationToFullNames setObject:@"Norma" forKey:@"Nor"];
    [constellAbbreviationToFullNames setObject:@"Octans" forKey:@"Oct"];
    [constellAbbreviationToFullNames setObject:@"Ophiuchus" forKey:@"Oph"];
    [constellAbbreviationToFullNames setObject:@"Orion" forKey:@"Ori"];
    [constellAbbreviationToFullNames setObject:@"Pavo" forKey:@"Pav"];
    [constellAbbreviationToFullNames setObject:@"Pegasus" forKey:@"Peg"];
    [constellAbbreviationToFullNames setObject:@"Perseus" forKey:@"Per"];
    [constellAbbreviationToFullNames setObject:@"Phoenix" forKey:@"Phe"];
    [constellAbbreviationToFullNames setObject:@"Pictor" forKey:@"Pic"];
    [constellAbbreviationToFullNames setObject:@"Pisces" forKey:@"Psc"];
    [constellAbbreviationToFullNames setObject:@"Piscis Austrinus" forKey:@"PsA"];
    [constellAbbreviationToFullNames setObject:@"Puppis" forKey:@"Pup"];
    [constellAbbreviationToFullNames setObject:@"Pyxis" forKey:@"Pyx"];
    [constellAbbreviationToFullNames setObject:@"Reticulum" forKey:@"Ret"];
    [constellAbbreviationToFullNames setObject:@"Sagitta" forKey:@"Sge"];
    [constellAbbreviationToFullNames setObject:@"Sagittarius" forKey:@"Sgr"];
    [constellAbbreviationToFullNames setObject:@"Scorpio" forKey:@"Sco"];
    [constellAbbreviationToFullNames setObject:@"Sculptor" forKey:@"Scl"];
    [constellAbbreviationToFullNames setObject:@"Scutum" forKey:@"Sct"];
    [constellAbbreviationToFullNames setObject:@"Serpens" forKey:@"Ser"];
    [constellAbbreviationToFullNames setObject:@"Sextans" forKey:@"Sex"];
    [constellAbbreviationToFullNames setObject:@"Taurus" forKey:@"Tau"];
    [constellAbbreviationToFullNames setObject:@"Telescopium" forKey:@"Tel"];
    [constellAbbreviationToFullNames setObject:@"Triangulum" forKey:@"Tri"];
    [constellAbbreviationToFullNames setObject:@"Triangulum Australe" forKey:@"TrA"];
    [constellAbbreviationToFullNames setObject:@"Tucana" forKey:@"Tuc"];
    [constellAbbreviationToFullNames setObject:@"Ursa Major" forKey:@"UMa"];
    [constellAbbreviationToFullNames setObject:@"Ursa Minor" forKey:@"UMi"];
    [constellAbbreviationToFullNames setObject:@"Vela" forKey:@"Vel"];
    [constellAbbreviationToFullNames setObject:@"Virgo" forKey:@"Vir"];
    [constellAbbreviationToFullNames setObject:@"Volans" forKey:@"Vol"];
    [constellAbbreviationToFullNames setObject:@"Vulpecula" forKey:@"Vul"];
    
}
@end
