//
//  ViewController.m
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "StarListViewController.h"
#import "StarsGraphicsViewController.h"
#import "DataStorage.h"
@interface ViewController ()
{
//    CLLocationManager *locationManager;
//    CLLocationCoordinate2D currentCoord;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //create location manager
    [GlobalLocationManager sharedManager];
    // Do any additional setup after loading the view, typically from a nib.
    [DataStorage sharedStorage];
     UIImageView* backgroundImage = [[UIImageView alloc] init];
    backgroundImage.frame = CGRectMake(backgroundImage.frame.origin.x, backgroundImage.frame.origin.y,self.view.bounds.size.height,self.view.bounds.size.height);
    backgroundImage.image =[UIImage imageNamed:@"stars_background.jpg"];
    float angleInDegrees = 90; // change this value to what you want
    float angleInRadians = angleInDegrees * (M_PI/180);
    backgroundImage.transform = CGAffineTransformMakeRotation(angleInRadians);
    [self.view insertSubview:backgroundImage atIndex:0];
    // Create the colors
    UIColor *topColor = [UIColor colorWithRed:100.0/255.0 green:0.0/255.0 blue:200.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:200.0/255.0 green:56.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theGradient = [CAGradientLayer layer];
    theGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    theGradient.frame = _constellations.bounds;
    [_constellations.layer addSublayer:theGradient];
    theGradient = [CAGradientLayer layer];
    theGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    theGradient.frame = _skyMap.bounds;
    [_skyMap.layer addSublayer:theGradient];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToStarsList:(id)sender {
    StarListViewController* ctrl = [[StarListViewController alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];

}

- (IBAction)toSky:(id)sender {
    StarsGraphicsViewController* ctrl = [[StarsGraphicsViewController alloc] init];
    ctrl.rotationEnabled = YES;
    ctrl.constellationIndex = -1;
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end
