//
//  StarsGraphicsView.h
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Metal/Metal.h>
#import <QuartzCore/CAMetalLayer.h>
@interface StarsGraphicsView : UIView
//@property (nonatomic, weak) id <AAPLViewDelegate> delegate;

// its own layer
@property (nonatomic) CAMetalLayer *metalLayer;

@end
