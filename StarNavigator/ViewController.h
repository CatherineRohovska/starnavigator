//
//  ViewController.h
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface ViewController : UIViewController
<CLLocationManagerDelegate>
- (IBAction)goToStarsList:(id)sender;
- (IBAction)toSky:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *constellations;

@property (weak, nonatomic) IBOutlet UIButton *skyMap;

@end

