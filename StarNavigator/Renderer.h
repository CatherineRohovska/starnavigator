//
//  Renderer.h
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <QuartzCore/CAMetalLayer.h>
#import <GLKit/GLKit.h>
#import "GlobalLocationManager.h"
@interface Renderer : NSObject
@property (nonatomic, readonly)  id <MTLDevice> device;
@property (nonatomic) CAMetalLayer* layer;
@property (nonatomic) MTLRenderPassDescriptor* renderDescriptor;
@property (nonatomic) float angle; //angle of rotation
@property (nonatomic) GLKVector3 rotationAxis;
@property (nonatomic) GLKVector3 lookAtVector; 
@property int constellationFocus;
- (instancetype)initWithLayer:(CAMetalLayer*)layer;
-(void) render;
@end
