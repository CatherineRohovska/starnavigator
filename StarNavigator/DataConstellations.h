//
//  DataConstellations.h
//  StarNavigator
//
//  Created by User on 10/9/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataConstellations : NSObject
@property(nonatomic) NSMutableDictionary* constellAbbreviationToFullNames;
@property(nonatomic) NSArray* constellationsCatalog;
+(id) sharedStorage;
@end
