//
//  StarDescription.m
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "StarDescription.h"

@implementation StarDescription
-(void) setStar:(NSDictionary *)star index: (int) starInGlobalData
{
    _name = [star objectForKey:@"Name"];
    _hr = [star objectForKey:@"HR"];
    _DEd = [[star objectForKey:@"DEd"] floatValue];
    _RAh = [[star objectForKey:@"RAh"] floatValue];
    _vmagnitude = [[star objectForKey:@"Vmag"] floatValue];
    _spectralClass = [star objectForKey:@"Sp"];
    NSString* tmpConstellation = [star objectForKey:@"bfID"];
    if (![tmpConstellation isEqualToString:@""])
    {_constellation = [tmpConstellation substringFromIndex:[tmpConstellation length]-3];
    }
    else
    {
        _constellation = @"";
    }
 
//   _celestialCoordinates.x = sin(M_PI - M_PI/180*(_RAh*15-23))*tmp;
//   _celestialCoordinates.y = Rsphere*sin((_DEd)*M_PI/180);
//   _celestialCoordinates.z = cos(M_PI/180*(_RAh*15-23))*tmp;
//   _celestialCoordinates.w = 1.0;
    _celestialCoordinates = [StarDescription calculateCoords:_DEd Acs:_RAh];
    _starIndex = starInGlobalData;

    
}
+(GLKVector4) calculateCoords: (float) DEd  Acs: (float) RAh{
    GLKVector4 tmpCoords;
    float tmp = fabs(Rsphere*cos((DEd)*M_PI/180));
    tmpCoords.x = sin(M_PI - M_PI/180*(RAh*15-23))*tmp;
    tmpCoords.y = Rsphere*sin((DEd)*M_PI/180);
    tmpCoords.z = cos(M_PI/180*(RAh*15-23))*tmp;
    tmpCoords.w = 1.0;
    return tmpCoords;
}

@end
