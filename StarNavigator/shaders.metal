//
//  shaders.metal
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
#ifdef __cplusplus

///stars data
typedef struct{
    packed_float4 position [[attribute(0)]];
    float pointSize [[attribute(1)]];
    float4 color [[attribute(2)]];
    
} graphicStarData;
///

    typedef struct
    {
        float4x4 projection_matrix;
        float4x4 modelview_matrix;
        
 
    } constants_t;

#endif



struct vertexOut {
    float4 position [[position]];
    float4 color;
    float pointSize [[point_size]];
};

// vertex shader function
vertex vertexOut lighting_vertex(device graphicStarData* vertex_array [[ buffer(0) ]],
                                  constant constants_t& constants [[ buffer(1) ]],
                                  unsigned int vid [[ vertex_id ]])
{
    vertexOut out;
    out.position = float4x4(constants.projection_matrix*constants.modelview_matrix)*float4(vertex_array[vid].position);
    out.pointSize = float(vertex_array[vid].pointSize);
    out.color = float4(vertex_array[vid].color);
   // out.color.a = 1;
    //out.color = {1.0f, 0.55f, 0.0f, 1.0f};
  //doing something with color input/output
    
    return out;
}

// fragment shader function
fragment float4 lighting_fragment(vertexOut in [[stage_in]])
{
    return in.color;//{1.0f, 0.0f, 0.0f, 1.0f};
};

