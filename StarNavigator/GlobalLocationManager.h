//
//  GlobalLocationManager.h
//  StarNavigator
//
//  Created by User on 10/7/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface GlobalLocationManager : NSObject
<CLLocationManagerDelegate>
@property (nonatomic,readonly) CLHeading* heading;
@property (nonatomic,readonly) CLLocationCoordinate2D coordinates;
@property (nonatomic) bool rotation;
+(id) sharedManager;
@end
