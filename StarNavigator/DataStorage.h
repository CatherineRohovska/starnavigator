//
//  DataStorage.h
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <math.h>
#import <GLKit/GLKit.h>
#import "StarDescription.h"
#import "ConstellationDescription.h"
typedef struct{
    GLKVector4 position;
    float pointSize;
    GLKVector4 color;
    
} graphicStarData;
const float Rsphere = 99.0;
@interface DataStorage : NSObject
{
    NSMutableArray* constellationsGlobalData;
    NSArray* starsOrder;
   
}
@property(nonatomic) NSMutableArray* data;
@property(nonatomic) NSMutableArray* constellations;
@property (nonatomic) graphicStarData* graphicData;
//@property (nonatomic)  NSMutableDictionary* constellAbbreviationToFullNames;
+(id) sharedStorage;
- (NSMutableArray*) getData;
@end
