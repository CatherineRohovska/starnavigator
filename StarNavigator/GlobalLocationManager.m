//
//  GlobalLocationManager.m
//  StarNavigator
//
//  Created by User on 10/7/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "GlobalLocationManager.h"
@interface GlobalLocationManager()
{
    CLLocationManager* locationManager;
}
@end
@implementation GlobalLocationManager
+(id) sharedManager{
    static GlobalLocationManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init])
    {
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _rotation = YES;
        [locationManager requestWhenInUseAuthorization];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            @synchronized(self)
            {
                [locationManager startUpdatingLocation];
                [locationManager startUpdatingHeading];
            }
           

        });
      
        
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation* location = [locations lastObject];
    _coordinates = location.coordinate;

}

- (void) locationManager:(CLLocationManager *)manager
        didUpdateHeading:(CLHeading *)newHeading{
    _heading = newHeading;
  
}
@end
