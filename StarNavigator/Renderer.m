//
//  Renderer.m
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "Renderer.h"
#import "DataStorage.h"
#import "ConstellationDescription.h"
//static const float kFOVY    = 180.0f;

static const GLKVector3 cameraPosition    = {0.0f, 0.0f, 0.0f};
static const GLKVector3 kUp     = {0.0f, 1.0f, 0.0f};

@implementation Renderer
{   float kFOVY; //angle of camera view
    id <MTLBuffer> _graphicMatrixBuffer;
    id <MTLDevice> _device;
    id <MTLCommandQueue> _commandQueue;
    id <MTLLibrary> _defaultLibrary;
    id <MTLRenderPipelineState> _pipelineState;
    id <MTLBuffer> _vertexBuffer;

    GLKMatrix4 _projectionMatrix;
    GLKMatrix4 _viewMatrix;
    GLKVector3 _lookAtPoint;
}

- (instancetype)initWithLayer:(CAMetalLayer*)layer
{
    self = [super init];
    if (self) {
        if (_constellationFocus!=-1) kFOVY = 20;
        else kFOVY = 360;

        _device = MTLCreateSystemDefaultDevice();
        _layer = layer;
        _layer.device = _device;
        _layer.pixelFormat = MTLPixelFormatBGRA8Unorm;
        _graphicMatrixBuffer =  [_device newBufferWithLength:sizeof(GLKMatrix4)*2 options:0];
        _graphicMatrixBuffer.label = @"MatrixBuffer";
        _commandQueue = [_device newCommandQueue];
        _defaultLibrary = [_device newDefaultLibrary];
        if(!_defaultLibrary) {
            NSLog(@">> ERROR: Couldnt create a default shader library");
         }
        //
        // get the fragment function from the library
        id <MTLFunction> fragmentProgram = [_defaultLibrary newFunctionWithName:@"lighting_fragment"];//some function
        if(!fragmentProgram)
            NSLog(@">> ERROR: Couldn't load fragment function from default library");
        
        // get the vertex function from the library
        id <MTLFunction> vertexProgram = [_defaultLibrary newFunctionWithName:@"lighting_vertex"]; // some function
        if(!vertexProgram)
            NSLog(@">> ERROR: Couldn't load vertex function from default library");

        //pipeline
        MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        
        pipelineStateDescriptor.label                           = @"NewPipeline";
        pipelineStateDescriptor.sampleCount                     = 1; //view.sampleCount; ??
        pipelineStateDescriptor.vertexFunction                  = vertexProgram;
        pipelineStateDescriptor.fragmentFunction                = fragmentProgram;
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm;
        pipelineStateDescriptor.colorAttachments[0].blendingEnabled = YES;
        pipelineStateDescriptor.colorAttachments[0].rgbBlendOperation = MTLBlendOperationAdd;
        pipelineStateDescriptor.colorAttachments[0].alphaBlendOperation = MTLBlendOperationAdd;
//
        pipelineStateDescriptor.colorAttachments[0].sourceRGBBlendFactor = MTLBlendFactorSourceAlpha;
        pipelineStateDescriptor.colorAttachments[0].destinationRGBBlendFactor = MTLBlendFactorSourceColor;
//
        pipelineStateDescriptor.colorAttachments[0].sourceAlphaBlendFactor = MTLBlendFactorSourceAlpha;
        pipelineStateDescriptor.colorAttachments[0].destinationAlphaBlendFactor = MTLBlendFactorOneMinusSourceAlpha;//
        
        // create a compiled pipeline state object. Shader functions (from the render pipeline descriptor)
    
        NSError *error = nil;
        _pipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
        if(!_pipelineState) {
            NSLog(@">> ERROR: Failed Aquiring pipeline state: %@", error);
            }
 
        [self createMatrices];
    }
    return self;
}
-(void) createMatrices
{
    //matrices
    float aspect = fabs([UIScreen mainScreen].bounds.size.width / [UIScreen mainScreen].bounds.size.height);
    _projectionMatrix = GLKMatrix4MakePerspective(kFOVY, aspect, 0.1f, 100.0f) ;
    _viewMatrix =GLKMatrix4MakeLookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z, _lookAtVector.x, _lookAtVector.y, _lookAtVector.z, kUp.x, kUp.y, kUp.z);
      // copy matrices to grafic buffer
    memcpy(_graphicMatrixBuffer.contents, &_projectionMatrix, sizeof(GLKMatrix4));
    memcpy(_graphicMatrixBuffer.contents+sizeof(GLKMatrix4), &_viewMatrix, sizeof(GLKMatrix4));

}
-(void) updateMatrices
{
    [self createMatrices];
    GlobalLocationManager* locationManager = [GlobalLocationManager sharedManager];
  //  calculating axis of rotation depending on latitude and longitude
    float tmp = fabs(100*cos((locationManager.coordinates.longitude)*M_PI/180));
    _rotationAxis.x = sin(M_PI - M_PI/180*(locationManager.coordinates.latitude))*tmp;
    _rotationAxis.y = 100*sin((locationManager.coordinates.latitude)*M_PI/180);
    _rotationAxis.z = cos(M_PI/180*(locationManager.coordinates.latitude))*tmp;
    //setup angle if rotation enabled
    if (locationManager.rotation)
    {
        self.angle = locationManager.heading.magneticHeading*M_PI/180;
    }
    // rotate matrix
    _viewMatrix = GLKMatrix4Rotate(_viewMatrix, self.angle, self.lookAtVector.x, self.lookAtVector.y, self.lookAtVector.z);
   
    memcpy(_graphicMatrixBuffer.contents, &_projectionMatrix, sizeof(GLKMatrix4));
    memcpy(_graphicMatrixBuffer.contents+sizeof(GLKMatrix4), &_viewMatrix, sizeof(GLKMatrix4));

}
-(void) render{
    //update matrices
    [self updateMatrices];
    
    DataStorage *dataStar = [DataStorage sharedStorage];
    GLKVector4 resetColor;
    if (_constellationFocus!=-1) //if constellation selected - change color of displaying stars
    {
        ConstellationDescription* constell = [dataStar.constellations objectAtIndex:_constellationFocus];
        for (int i=0; i<constell.starsCount; i++)
        {//index of star saved in array of indexes
            resetColor = GLKVector4Make(dataStar.graphicData[constell.starsIndexes[i]].color.r, dataStar.graphicData[constell.starsIndexes[i]].color.g, dataStar.graphicData[constell.starsIndexes[i]].color.b, dataStar.graphicData[constell.starsIndexes[i]].color.a);
            dataStar.graphicData[constell.starsIndexes[i]].color.r = 1.0f;
            dataStar.graphicData[constell.starsIndexes[i]].color.g =0.0f;
            dataStar.graphicData[constell.starsIndexes[i]].color.b =1.0f;
            dataStar.graphicData[constell.starsIndexes[i]].color.a = 1.0f;
 
        }
    }
    //create vertex buffer
    _vertexBuffer = [_device newBufferWithBytes:dataStar.graphicData
                                         length:sizeof(graphicStarData)*[dataStar getData].count
                                        options:MTLResourceOptionCPUCacheModeDefault];
    //reset color of displaying stars
    if (_constellationFocus!=-1)
    {
        ConstellationDescription* constell = [dataStar.constellations objectAtIndex:_constellationFocus];
        for (int i=0; i<constell.starsCount; i++)
        {//index of star saved in array of indexes
            dataStar.graphicData[constell.starsIndexes[i]].color.r = resetColor.r;
            dataStar.graphicData[constell.starsIndexes[i]].color.g =resetColor.g;
            dataStar.graphicData[constell.starsIndexes[i]].color.b =resetColor.b;
            dataStar.graphicData[constell.starsIndexes[i]].color.a = resetColor.a;
          
        }
    }
    _vertexBuffer.label = @"Vertices";
    /// now graphicData does not have recoloured points
    id <CAMetalDrawable> currentDrawable = [_layer nextDrawable];
    if (!currentDrawable) {return;}
        _renderDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
     
        // create a color attachment every frame since we have to recreate the texture every frame
        MTLRenderPassColorAttachmentDescriptor *colorAttachment = _renderDescriptor.colorAttachments[0];
        
        colorAttachment.texture = [currentDrawable texture];
        
        // make sure to clear every frame for best performance
        colorAttachment.loadAction = MTLLoadActionClear;
        colorAttachment.clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
        colorAttachment.storeAction = MTLStoreActionStore;
   

        id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];

       id <MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:_renderDescriptor];
        [renderEncoder pushDebugGroup:@"Stars"];
      //  [renderEncoder setDepthStencilState:_depthState];
        [renderEncoder setRenderPipelineState:_pipelineState];
        [renderEncoder setVertexBuffer:_vertexBuffer offset:0 atIndex:0 ];
    
        [renderEncoder setVertexBuffer:_graphicMatrixBuffer offset:0 atIndex:1 ];
            
            // tell the render context we want to draw our primitives
        DataStorage* dataStorage = [DataStorage sharedStorage];
        //points to  count of stars and creating matrices
    
        [renderEncoder drawPrimitives:MTLPrimitiveTypePoint vertexStart:0 vertexCount:[dataStorage getData].count];
        //draw lines

    //draw lines if constellation selected
    if (_constellationFocus!=-1)
    {
      ConstellationDescription* constell = [dataStar.constellations objectAtIndex:_constellationFocus];
        
        for (int i=0; i<constell.starsOrder.count; i++)
      {//index of star saved in array of  array indexes
          graphicStarData* singleline = malloc(sizeof(graphicStarData)*2);
                     singleline[0].color.r = 1.0f;
                      singleline[0].color.g = 0.0f;
                      singleline[0].color.b = 0.0f;
                      singleline[1].color.r = 1.0f;
                      singleline[1].color.g = 0.0f;
                      singleline[1].color.b = 0.0f;
                      singleline[1].color.a = singleline[0].color.a = 1;
                      singleline[0].pointSize = 2;
                      singleline[1].pointSize = 2;
          singleline[0].position = constell.linesCoord[i].firstCoodinate;
          singleline[1].position = constell.linesCoord[i].secondCoordinate;
          _vertexBuffer = [_device newBufferWithBytes:singleline
                                   length:sizeof(graphicStarData)*2
                                   options:MTLResourceOptionCPUCacheModeDefault];
                   [renderEncoder setVertexBuffer:_vertexBuffer offset:0 atIndex:0 ];
                   [renderEncoder drawPrimitives:MTLPrimitiveTypeLine vertexStart:0 vertexCount:2];
          
       }
//
    }
    else{ //we want to display all constellations at sphere
    //
    for (int i=0; i<dataStorage.constellations.count; i++) {
   
    ConstellationDescription* constell = [dataStar.constellations objectAtIndex:i];
        for (int i=0; i<constell.starsOrder.count; i++)
    {//index of star saved in array of  array indexes
        graphicStarData* singleline = malloc(sizeof(graphicStarData)*2);
        singleline[0].color.r = 0.0f;
        singleline[0].color.g = 0.0f;
        singleline[0].color.b = 1.0f;
        singleline[1].color.r = 0.0f;
        singleline[1].color.g = 0.0f;
        singleline[1].color.b = 1.0f;
        singleline[1].color.a = singleline[0].color.a = 0.3;
        singleline[0].pointSize = 1;
        singleline[1].pointSize = 1;
        singleline[0].position = constell.linesCoord[i].firstCoodinate;
        singleline[1].position = constell.linesCoord[i].secondCoordinate;
        _vertexBuffer = [_device newBufferWithBytes:singleline
                                             length:sizeof(graphicStarData)*2
                                            options:MTLResourceOptionCPUCacheModeDefault];
        [renderEncoder setVertexBuffer:_vertexBuffer offset:0 atIndex:0 ];
        [renderEncoder drawPrimitives:MTLPrimitiveTypeLine vertexStart:0 vertexCount:2];
        
    }
    }
    }
   
    //
        [renderEncoder endEncoding];
        [renderEncoder popDebugGroup];
    
        // schedule a present once rendering to the framebuffer is complete
        [commandBuffer presentDrawable:currentDrawable];
        [commandBuffer commit];
    
}
@end
