//
//  StarsGraphicsViewController.h
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "GlobalLocationManager.h"
@interface StarsGraphicsViewController : UIViewController

// the time interval from the last draw
@property (nonatomic, readonly) NSTimeInterval timeSinceLastDraw;

// What vsync refresh interval to fire at. (Sets CADisplayLink frameinterval property)
// set to 1 by default, which is the CADisplayLink default setting (60 FPS).
// Setting to 2 -  (throttling to 30 FPS)
@property (nonatomic) NSUInteger interval;
@property (nonatomic) bool rotationEnabled;
@property (nonatomic) int constellationIndex;
@end
