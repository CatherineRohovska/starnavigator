//
//  StarsGraphicsViewController.m
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "StarsGraphicsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "StarsGraphicsView.h"
#import "Renderer.h"
#import "DataStorage.h"
#import "ConstellationDescription.h"

@interface StarsGraphicsViewController ()
{
    CADisplayLink *_timer;
    Renderer* renderer;
    CFTimeInterval _timeSinceLastDrawPreviousTime;
   
 
    float direction;

}

@end

@implementation StarsGraphicsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _timer = [CADisplayLink displayLinkWithTarget:self
                                                 selector:@selector(loops)];
    
    [_timer addToRunLoop:[NSRunLoop currentRunLoop] //[NSRunLoop mainRunLoop]
                 forMode:NSDefaultRunLoopMode];
  
    StarsGraphicsView *renderView = [[StarsGraphicsView alloc] initWithFrame:self.view.bounds];
    renderer = [[Renderer alloc] initWithLayer:renderView.metalLayer]; //init renderer with drawable layer
    if (_constellationIndex!=-1) //if we want to display constellation
    {
        DataStorage* dataStorage = [DataStorage sharedStorage];
        ConstellationDescription* constell = dataStorage.constellations[_constellationIndex];
        renderer.lookAtVector = GLKVector3Make(constell.coordinatesCenter.x, constell.coordinatesCenter.y, constell.coordinatesCenter.z);
        renderer.constellationFocus = _constellationIndex;
    }
    else
    {
        GlobalLocationManager* locationManager = [GlobalLocationManager sharedManager];
        float tmp = fabs(100*cos((locationManager.coordinates.latitude)*M_PI/180));
        float x = sin(M_PI - M_PI/180*(locationManager.coordinates.longitude-23))*tmp;
        float y = 100*sin((locationManager.coordinates.latitude)*M_PI/180);
        float z = cos(M_PI/180*(locationManager.coordinates.longitude-23))*tmp;
        renderer.lookAtVector = GLKVector3Make(x, y, z);// {0.0f, 0.0f, 1.0f};
        renderer.constellationFocus = -1;
       // locationManager.rotation = YES;
    }
    renderView.autoresizingMask =(UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth);
    [self.view addSubview:renderView];
 
    GlobalLocationManager* locationManager = [GlobalLocationManager sharedManager];
    locationManager.rotation = _rotationEnabled;

    
//
    
}
-(void) rotateCamera //camera rotation ob click
{
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initCommon
{
   
    _interval = 1;
}
- (id)init
{
    self = [super init];
    
    if(self)
    {
        [self initCommon];
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
       [_timer invalidate];
    _timer = nil;

}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIApplicationDidEnterBackgroundNotification
                                                  object: nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIApplicationWillEnterForegroundNotification
                                                  object: nil];
    NSLog(@"dealloc");

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)loops // main loop of view
{
    
    
        CFTimeInterval currentTime = CACurrentMediaTime();
        
        _timeSinceLastDraw = currentTime - _timeSinceLastDrawPreviousTime;
        
        // keep track of the time interval between draws
        _timeSinceLastDrawPreviousTime = currentTime;

    [renderer render];
   
}

@end
