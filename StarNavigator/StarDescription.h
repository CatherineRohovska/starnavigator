//
//  StarDescription.h
//  StarNavigator
//
//  Created by User on 10/2/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "DataStorage.h"
#import "GlobalLocationManager.h"
@interface StarDescription : NSObject
{
  
}
@property(nonatomic,readonly) NSString* name;
@property(nonatomic,readonly) NSString* hr;
@property(nonatomic,readonly) float RAh; //прямое восхождение
@property(nonatomic,readonly) float DEd;// склонение
@property(nonatomic,readonly) float vmagnitude;
@property (nonatomic, readonly) NSString* spectralClass;
@property (nonatomic, readonly) NSString* constellation;
@property (nonatomic,readonly) GLKVector4 celestialCoordinates;
@property(nonatomic, readonly) int starIndex;
- (void) setStar: (NSDictionary*) star index: (int) starInGlobalData;
+ (GLKVector4) calculateCoords: (float) DEd  Acs: (float) RAh;
@end


