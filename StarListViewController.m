//
//  StarListViewController.m
//  StarNavigate
//
//  Created by User on 10/1/15.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "StarListViewController.h"
#import "DataStorage.h"
#import "StarDescription.h"
#import "ConstellationDescription.h"
#import "StarsGraphicsViewController.h"
@interface StarListViewController ()
{
    UITableView* starsTable;
    NSMutableArray* starsData;
    NSMutableArray* constellations;
    
}

@end

@implementation StarListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //creating table
    starsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    starsTable.delegate = self;
    starsTable.dataSource = self;
    [starsTable registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Star"];
    starsTable.autoresizingMask =(UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth);

    [self.view addSubview:starsTable];
    DataStorage* dataStorage = [DataStorage sharedStorage];
    starsData = [[DataStorage sharedStorage] getData];
    constellations = dataStorage.constellations;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
 return constellations.count;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Star" forIndexPath:indexPath];
  //  StarDescription* item = [starsData objectAtIndex:indexPath.item];
    ConstellationDescription* constellation = [constellations objectAtIndex:indexPath.item];
    NSString* name;
    // starsData sor
   // StarDescription* star
    if (!constellation.fullName) name = constellation.name;
    else
    name = constellation.fullName;//[item objectForKey:@"Name"];

    [cell.textLabel setText:name];

    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    //    Screen9 *ctrl = [storyboard instantiateViewControllerWithIdentifier:@"Screen9"];
    //    ctrl.bookmark = (int)indexPath.item; //adding mark to scroll CollectionView
    //  NSLog(@"%ld",(long)ctrl.bookmark);
   
    StarsGraphicsViewController* ctrl = [[StarsGraphicsViewController alloc] init];
    ctrl.rotationEnabled = NO;
    ctrl.constellationIndex = (int)indexPath.item;
    [self.navigationController pushViewController:ctrl animated:YES];
}

@end
